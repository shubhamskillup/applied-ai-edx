## Exploring Watson Visual Recognition

IBM provides an online demo of Watson Visual Recognition at: [Watson Visual Recognition](https://www.ibm.com/cloud/watson-visual-recognition#demo) (or from the product page, click Demo).  

The demo environment provides two models for analysis:

* Custom Models, which businesses use to recognize any object, scene or attribute to solve their unique challenges.

* Pre Trained Models, which are category specific to enable you to analyze images for scenes, objects, faces, colors, food, and other content.

In this demo, you will use Watson Visual Recognition to analyze images, first with the Custom Model, and then with the Pre Trained Models.



Use the following steps to explore the demo:


### Custom Model

1. Access the online demo here: [Watson Visual Recognition](https://www.ibm.com/cloud/watson-visual-recognition#demo)
2. Close the message box that begins “This system is for demonstration purposes only….”
3. Look at the image - what do you see?
4. Under <strong>Insurance (Custom Classifier)</strong>, review the options.
5. What level of confidence does Watson have that the image is of:
6. a flat tire
7. vandalism
8. a broken windshield
9. a motorcycle accident
10. Repeat this process for each of the sample images.


### Pre Trained Models

1. Select the image of the person in the tweed jacket.
2. Under General Model, review the options. Note that Watson has identified characteristics that exist in the image.
3. What level of confidence does Watson have that the image is of:
4. fabric
5. gray color
6. Harris Tweed (jacket)
7. clothing
8. Select the image of liquid in a beaker.
9. Under General Model, review the options. Watson can identify characteristics that exist in the image.
10. What level of confidence does Watson have that the image is of:
11. chocolate color
12. beverage
13. food
14. Expand <strong>Food Model</strong>. Watson can Identify meals and food items with enhanced accuracy.
15. What level of confidence does Watson have that the image is of:
16. a non-food
17. Repeat this process for each image.
18. Expand <strong>Explicit Model</strong>. Watson can determine if an image contains nudity or other inappropriate content. 
19. What level of confidence does Watson have that the image does not contain explicit content?
20. Repeat this process for each image.


### Optional

1. Choose an image file of your own and drag it to the <strong>Drop Image here or Click to Upload</strong> box.
2. Examine the results.









