# Exercise: Using AutoAI to predict outcomes!

You're on the marketing team for a bank and your team is launching a new campaign. In order to launch the most effective campaign, you need to determine who to target in the campaign based on the likelihood that your target audience will respond to the messaging.

In this hands-on exercise you will utilize Watson AutoAI to predict the likelihood of a particular outcome based on input data. You'll create and train a model using sample data and then provide input data to your model see how likely or unlikely a particular outcome will be.

NOTE: For learners working to get a certificate for this course (i.e. not auditing), please go through this exercise carefully and thoroughly it will be used for the graded assignment that follows.

FYI: In order to complete this exercise you will be creating an IBM Cloud account and provisioning an instance for Watson Studio and Machine Learning services. A credit card is NOT required to sign up for IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance of the Watson Studio or Machine Learning services.

If you already have an IBM Cloud Account you can skip to step 2.

## Step 1: Create an IBM Cloud Account 

1. Go to: Create a free account on IBM Cloud

2. In the Email box, enter your email address and then click the arrow.

3. When your email address is accepted, enter your First Name, Last Name, Country or Region, and create a Password.

Note: To get enhanced benefits, please sign up with your company email address rather than a free email ID like Gmail, Hotmail, etc.

If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the option to be notified by email.

4. Click Create Account to create your IBM Cloud account.

Task 2: Confirm your email address

1. An email is sent to the address that you signed up with.



2. Check your email, and in the email that was sent to you, click Confirm Account.



3. You will receive notification that your account is confirmed.



Click Log In, and you will be directed to the IBM Cloud Login Page.

Task 3: Login to your IBM Cloud account

1. On the Log in to IBM Cloud page, in the ID box, enter your email address and then click Continue. 



2. In the Password box, enter your password, and then click Log in.



Step 2: Add a Machine Learning Service Instance

Next we need to add an instance of the Machine Learning service to your account. This will be used later when you set up the rest of the assets for the project. From your Dashboard page, click or tap the Create Resource button at the top right of the page.



From the catalog page that loads, select the AI category and then select the Machine Learning service from the list on the right.



In the Machine Learning setup page, ensure the Lite plan is selected and then click or tap Create. Note: If you created a Machine Learning instance in the past using the Lite plan, you won't be allowed to create a second instance. You can use the instance you already created for this exercise or you'll need to delete your previous instance and create a new one.

 

 

Step 3: Create a Watson Studio Resource

In this step, you'll be using Watson Studio to see AI in action. If you already have a Watson Studio resource set up, you can skip to step 4.

To manage all your projects, you will use IBM Watson Studio. In this exercise, you will add Watson Studio as a Resource.

Task 1: Add Watson Studio as a resource

1. On the Dashboard, click Create Resource.



2. In the Catalog, click AI (16).



Note that the Lite Pricing plan is selected.

3. In the list of Services, click Watson Studio.



4. On the Watson Studio page, select the region closest to you, verify that the Lite plan is selected, and then click Create.



5. When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click Get Started.



6. You will see this message when Watson Studio is successfully set up for you.



 

Step 4: Create a Project

On the Watson Studio welcome page, click Create a Project to start the new project flow. Choose the Create an empty project option.



On the New Project page, enter a name for your project (we suggest using "Auto AI Exercise" or something similar) and, if you'd like, a description.



Step 5: Add Auto AI Asset to Your Project

On the project page, click the Add to project button at the top of the page to add a new asset to your project.



On the Choose asset type page, select the AutoAI experiment asset to add this to your project.



On the Create an AutoAI experiment page, choose From sample and the Bank marketing sample should auto select for you.

 
In order to use AutoAI, you will need to associate a machine learning service with your project. If a machine learning service is not associated with your project, you can associate one under the Associate services section on this page. Click the link, Associate a Machine Learning service instance to start the association flow. Associate the Machine Learning service you created in an earlier step in this exercise and click Select.



After the service is associated, you will be taken back to the experiment details page. Click or tap the Reload button to refresh the machine learning association. Your machine learning instance should now show under the "Associate services" section.

Now click the Create button and your AutoAI asset will be created in your project.



Step 6: Run the Experiment

When the "Bank marketing experiment" page loads, you can click next on all the wizard buttons to close out the wizard. Now you're ready to analyze the sample data. Click the Run experiment button.



Let the classifier run until it's complete. You will see a visual representation of the work being done. You see a message on the right-hand side of the page when the experiment is complete. In the diagram on the left of the page, you'll see various pipelines that represent the experiments that were run. One pipeline will be starred. This shows the most effective experiment.



Step 7: Save the Model

On the experiment summary page, scroll down to the Pipeline leader board. Hover over the pipeline that has the first ranking in the list until the Save as Model button appears and then click the button.



 

You can keep the default name or create your own and add a description if you want to. When you're ready, click the Save button to save your model.



Step 8: View the Model

To view the model you just created, navigate back to the project by clicking project name in the "breadcrumb" menu at the top as shown in the screenshot (note: your project name may be different than the one shown).



Once on the project page, scroll down to the models section and find the model you saved and click the link (the name of the model) to open it.



Step 9: Create a Deployment

In this step, you're going to deploy the model you just created to a web service so it can be take input data. On the model page, click the Deployments tab and then click the Add Deployment button.

 

On the deployment details page, give the deployment a name and, if you'd like, add a description. Once you're done, click Save. The deployment may go through various stages until it's ready. You may need to refresh your browser to update the status. Once the deployment status states "ready," click on the deployment name to open it.



Step 10: Test Your Deployment

Now that you have your model deployed, you're ready to test it with some data. On the deployment page you just opened, click the Test tab. You're going to paste in some JSON instead of entering data into the form so click on the glyph that looks like a piece of paper to load the payload interface.



To test the model, paste in the following JSON code and click the Predict button at the bottom.

{"input_data":[{ "fields": ["age","job","marital","education","default","balance","housing","loan","contact","day", "month","duration","campaign","pdays","previous","poutcome"], "values": [[27,"unemployed", "married", "primary", "no",1787,"no", "no","cellular",19,"oct", 79, 1, -1, 0, "unknown" ]] }]}

For reference, here is the same JSON data formatting so it’s easier to read. The “fields” are at the top of the JSON and the values for those fields are at the bottom.

{

  "input_data": [

    {

      "fields": [

        "age",

        "job",

        "marital",

        "education",

        "default",

        "balance",

        "housing",

        "loan",

        "contact",

        "day",

        "month",

        "duration",

        "campaign",

        "pdays",

        "previous",

        "poutcome"

      ],

      "values": [

        [

          27,

          "unemployed",

          "married",

          "primary",

          "no",

          1787,

          "no",

          "no",

          "cellular",

          19,

          "oct",

          79,

          1,

          -1,

          0,

          "unknown"

        ]

      ]

    }

  ]

}

The return values highlighted in the screenshot below indicate that based on the input values, this individual is unlikely to enroll in a marketing promotion. The result gives us the prediction (outlined in red) and the probability based on the model which is using the input data (in green). What the results tell us is that the model predicts with roughly 92% probability (highlighted in green) that, based on the input data for this individual, he or she would not respond to the marketing effort (highlighted in red).



You can modify the input data to try different predictions. The first section of the JSON contains the fields and the second section contains the values for those fields. For example, the second field is "job" and in the example data, the individual is "unemployed." Try changing this to "employed" and see how it affects the outcome. As you prepare for your final project, do the following

Take a screenshot of your modified input and the result and save it to your local computer (similar to the screenshot in this step; you don’t have to color code the prediction and probability however).
For practice, you can change values two more times, run the model, and take and save screenshots after each run and save them to your computer.
We just used AI machine learning to predict how a customer is likely to respond to an effort by a bank to get their customers to enroll in a program using a marketing pitch! Can you think of ways a real bank could use this information to help them run their marketing campaigns?

Step 11: Share Your Results!

Follow us on Twitter and send us some of your funniest and most interesting results you found with IBM Watson AutoAI!

 

 Click here to share the above Tweet.

Follow Rav Ahuja